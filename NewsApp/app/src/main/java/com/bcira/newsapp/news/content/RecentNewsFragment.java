package com.bcira.newsapp.news.content;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bcira.newsapp.R;
import com.bcira.newsapp.data.response.Article;
import com.bcira.newsapp.util.Injector;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class RecentNewsFragment extends Fragment implements RecentNewsStateView, ItemClickListener {

    private static final String KEY = "key";

    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.recyclerview_news)
    RecyclerView recyclerView;

    @Inject
    RecentNewsPresenter presenter;

    private Unbinder unbinder;
    private ProgressDialog progressDialog;
    private RecentNewsState state;

    public static RecentNewsFragment newInstance() {
        Bundle args = new Bundle();
        RecentNewsFragment fragment = new RecentNewsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Injector.createRecentNewsComponent(this).inject(this);
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            state = new RecentNewsState.Builder()
                    .isJustInitialized(true)
                    .build();
        } else {
            state = savedInstanceState.getParcelable(KEY);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.takeView(this);
        presenter.updateToolbarTile();
        presenter.retrieveNews(state);
    }

    @Override
    public void displayNews(RecentNewsState state) {
        this.state = state;

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.Adapter adapter = new NewsAdapter(getContext(), state.getResponse().getArticles(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void displayError(String message) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(getString(R.string.ok), v -> snackbar.dismiss());
        snackbar.show();
    }

    @Override
    public void onItemClick(Article article) {
        presenter.openDetailsScreen(article);
    }

    @Override
    public void displayProgress(boolean shouldDisplayProgress) {
        if (shouldDisplayProgress) {
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage(getString(R.string.loading));
            progressDialog.show();
        } else if (progressDialog != null) {
            progressDialog.cancel();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY, new RecentNewsState.Builder(state)
                .isRestarted(true)
                .build());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.dropView(this);
        }
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        Injector.releaseRecentNewsComponent();
        super.onDestroy();
    }
}
