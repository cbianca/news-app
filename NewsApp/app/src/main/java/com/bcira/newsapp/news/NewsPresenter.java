package com.bcira.newsapp.news;

import com.bcira.newsapp.common.BasePresenter;
import com.bcira.newsapp.data.response.Article;

import javax.inject.Inject;

class NewsPresenter extends BasePresenter<NewsStateView> {

    private NewsWireframe wireframe;

    @Inject
    NewsPresenter(NewsWireframe wireframe) {
        this.wireframe = wireframe;
    }

    void openNewsScreen(boolean isRestarted) {
        if (!isRestarted) {
            wireframe.startNewsScreen();
        }
    }

    void openDetailsScreen(Article article) {
        wireframe.startDetailsScreen(article);
    }
}
