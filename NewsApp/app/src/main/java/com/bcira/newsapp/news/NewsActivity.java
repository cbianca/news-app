package com.bcira.newsapp.news;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.bcira.newsapp.R;
import com.bcira.newsapp.data.response.Article;
import com.bcira.newsapp.util.Injector;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.content.pm.ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

public class NewsActivity extends AppCompatActivity implements NewsStateView, ToolbarTitleListener,
        ScreenOpenerListener {

    private static final String KEY = "key";

    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @Nullable
    @BindView(R.id.textview_toolbar_title)
    protected TextView toolbarTitleTextView;

    @Inject
    NewsPresenter presenter;

    private Unbinder unbinder;
    private NewsState state;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Injector.createNewsComponent(this).inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        if (getResources().getBoolean(R.bool.portrait)) {
            setRequestedOrientation(SCREEN_ORIENTATION_PORTRAIT);
        }

        if (savedInstanceState == null) {
            state = new NewsState.Builder().build();
        } else {
            state = savedInstanceState.getParcelable(KEY);
        }

        unbinder = ButterKnife.bind(this);
        presenter.takeView(this);
        presenter.openNewsScreen(state.isRestarted());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean isHandled = false;
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                isHandled = true;
                break;
            }
            default: {
                break;
            }
        }
        return isHandled || super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onTitleChanged(int titleRes, boolean isHomeButtonEnabled) {
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.setDisplayHomeAsUpEnabled(isHomeButtonEnabled);
                supportActionBar.setHomeButtonEnabled(true);
                supportActionBar.setDisplayShowTitleEnabled(false);
                if (toolbarTitleTextView != null) {
                    toolbarTitleTextView.setText(titleRes);
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY, new NewsState.Builder()
                .isRestarted(true)
                .build());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.dropView(this);
        if (unbinder != null) {
            unbinder.unbind();
        }
        Injector.releaseNewsComponent();
    }

    @Override
    public void openDetailsScreen(Article article) {
        presenter.openDetailsScreen(article);
    }
}
