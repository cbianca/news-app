package com.bcira.newsapp.news;

public interface ToolbarTitleListener {

    void onTitleChanged(int titleRes, boolean isHomeButtonEnabled);
}
