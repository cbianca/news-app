package com.bcira.newsapp.news.content;

import android.os.Parcel;
import android.os.Parcelable;

import com.bcira.newsapp.data.response.NewsResponse;
import com.bcira.newsapp.news.NewsState;

public class RecentNewsState extends NewsState implements Parcelable {

    public static final Creator<RecentNewsState> CREATOR = new Creator<RecentNewsState>() {
        @Override
        public RecentNewsState createFromParcel(Parcel source) {
            return new RecentNewsState(source);
        }

        @Override
        public RecentNewsState[] newArray(int size) {
            return new RecentNewsState[size];
        }
    };
    private boolean isJustInitialized;
    private NewsResponse response;

    private RecentNewsState(Builder builder) {
        super(builder);
        isJustInitialized = builder.isJustInitialized;
        response = builder.response;
    }

    protected RecentNewsState(Parcel in) {
        super(in);
        this.isJustInitialized = in.readByte() != 0;
        this.response = in.readParcelable(NewsResponse.class.getClassLoader());
    }

    public boolean isJustInitialized() {
        return isJustInitialized;
    }

    public NewsResponse getResponse() {
        return response;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte(this.isJustInitialized ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.response, flags);
    }

    public static final class Builder extends NewsState.Builder {
        private boolean isJustInitialized;
        private NewsResponse response;

        public Builder() {
        }

        public Builder(RecentNewsState copy) {
            this.isJustInitialized = copy.isJustInitialized();
            this.response = copy.getResponse();
        }

        public Builder isJustInitialized(boolean val) {
            isJustInitialized = val;
            return this;
        }

        public Builder withResponse(NewsResponse val) {
            response = val;
            return this;
        }

        public RecentNewsState build() {
            return new RecentNewsState(this);
        }
    }
}
