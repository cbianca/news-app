package com.bcira.newsapp.data;

import com.bcira.newsapp.data.response.NewsResponse;
import com.bcira.newsapp.util.RxSchedulers;
import com.bcira.newsapp.util.Settings;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Single;

public class CryptoCoinsNewsRepository {

    private CryptoCoinsNewsApi cryptoCoinsNewsApi;
    private RxSchedulers schedulers;

    @Inject
    public CryptoCoinsNewsRepository(CryptoCoinsNewsApi cryptoCoinsNewsApi,
                                     RxSchedulers schedulers) {
        this.cryptoCoinsNewsApi = cryptoCoinsNewsApi;
        this.schedulers = schedulers;
    }

    Single<NewsResponse> observeNews() {
        Map<String, String> map = new HashMap<>();
        map.put("sources", Settings.SOURCES);
        map.put("apiKey", Settings.API_KEY);

        return cryptoCoinsNewsApi.news(map)
                .subscribeOn(schedulers.io())
                .observeOn(schedulers.androidUI());
    }
}
