package com.bcira.newsapp.news;

import com.bcira.newsapp.data.response.Article;

public interface NewsWireframe {

    void startNewsScreen();

    void startDetailsScreen(Article article);
}
