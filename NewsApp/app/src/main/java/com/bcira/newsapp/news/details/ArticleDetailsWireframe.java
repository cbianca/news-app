package com.bcira.newsapp.news.details;

public interface ArticleDetailsWireframe {

    void updateToolbarTitle(int titleRes, boolean isHomeButtonEnabled);
}
