package com.bcira.newsapp.news.details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bcira.newsapp.R;
import com.bcira.newsapp.data.response.Article;
import com.bcira.newsapp.util.Injector;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ArticleDetailsFragment extends Fragment implements ArticleStateView {

    public static final String TAG = ArticleDetailsFragment.class.getSimpleName();

    private static final String ARG_ARTICLE = "arg_article";

    @BindView(R.id.textview_article_date)
    TextView dateTextView;
    @BindView(R.id.textview_article_title)
    TextView titleTextView;
    @BindView(R.id.textview_article_author)
    TextView authorTextView;
    @BindView(R.id.textview_article_content)
    TextView contentTextView;
    @BindView(R.id.imageview_article)
    ImageView articleImageView;

    @Inject
    ArticleDetailsPresenter presenter;

    private Unbinder unbinder;

    public static ArticleDetailsFragment newInstance(Article article) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_ARTICLE, article);
        ArticleDetailsFragment fragment = new ArticleDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Injector.createArticleComponent(this).inject(this);
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.takeView(this);
        presenter.updateToolbarTile();
        displayArticle(getArguments().getParcelable(ARG_ARTICLE));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (presenter != null) {
            presenter.dropView(this);
        }
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        Injector.releaseArticleComponent();
        super.onDestroy();
    }

    private void displayArticle(Article article) {
        presenter.convertDate(article.getPublishedDate());
        titleTextView.setText(article.getTitle());
        authorTextView.setText(article.getAuthor() != null ? article.getAuthor() : getString(R.string.anonymous));
        contentTextView.setText(article.getContent());

        Picasso.get()
                .load(article.getUrlToImage())
                .placeholder(R.drawable.placeholder)
                .into(articleImageView);
    }

    @Override
    public void displayPublishedDate(String dateStr) {
        dateTextView.setText(dateStr);
    }
}
