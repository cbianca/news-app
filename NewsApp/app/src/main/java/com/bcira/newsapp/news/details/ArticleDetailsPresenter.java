package com.bcira.newsapp.news.details;

import android.annotation.SuppressLint;

import com.bcira.newsapp.R;
import com.bcira.newsapp.common.BasePresenter;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

@SuppressLint("SimpleDateFormat")
public class ArticleDetailsPresenter extends BasePresenter<ArticleStateView> {

    private static final String PATTERN = "dd MMM yyyy";
    private ArticleDetailsWireframe wireframe;
    private SimpleDateFormat dateFormat;

    @Inject
    ArticleDetailsPresenter(ArticleDetailsWireframe wireframe) {
        this.wireframe = wireframe;
        dateFormat = new SimpleDateFormat(PATTERN);
    }

    void updateToolbarTile() {
        wireframe.updateToolbarTitle(R.string.article, true);
    }

    void convertDate(Date date) {
        getView().displayPublishedDate(dateFormat.format(date));
    }
}
