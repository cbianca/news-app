package com.bcira.newsapp.news.details;

interface ArticleStateView {

    void displayPublishedDate(String dateStr);
}
