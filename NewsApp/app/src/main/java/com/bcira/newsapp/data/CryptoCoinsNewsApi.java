package com.bcira.newsapp.data;

import com.bcira.newsapp.data.response.NewsResponse;

import java.util.Map;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface CryptoCoinsNewsApi {

    @GET("everything")
    Single<NewsResponse> news(@QueryMap Map<String, String> params);
}
