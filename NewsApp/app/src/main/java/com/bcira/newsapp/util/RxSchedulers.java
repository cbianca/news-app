package com.bcira.newsapp.util;

import io.reactivex.Scheduler;

public interface RxSchedulers {

    Scheduler androidUI();

    Scheduler io();
}
