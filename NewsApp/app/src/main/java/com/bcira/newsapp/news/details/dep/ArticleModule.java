package com.bcira.newsapp.news.details.dep;

import com.bcira.newsapp.news.details.ArticleDetailsFragment;
import com.bcira.newsapp.news.details.ArticleDetailsWireframe;
import com.bcira.newsapp.news.details.DefaultArticleDetailsWireframe;

import dagger.Module;
import dagger.Provides;

@Module
public class ArticleModule {

    private ArticleDetailsFragment fragment;

    public ArticleModule(ArticleDetailsFragment fragment) {
        this.fragment = fragment;
    }

    @ArticleScope
    @Provides
    ArticleDetailsWireframe providesArticleDetailsWireframe() {
        return new DefaultArticleDetailsWireframe(fragment);
    }
}
