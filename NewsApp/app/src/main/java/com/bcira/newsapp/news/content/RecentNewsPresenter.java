package com.bcira.newsapp.news.content;

import com.bcira.newsapp.R;
import com.bcira.newsapp.common.BasePresenter;
import com.bcira.newsapp.data.DataSourceRepository;
import com.bcira.newsapp.data.response.Article;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class RecentNewsPresenter extends BasePresenter<RecentNewsStateView> {

    private DataSourceRepository dataSourceRepository;
    private RecentNewsWireframe wireframe;
    private Disposable newsDisposable;

    @Inject
    RecentNewsPresenter(DataSourceRepository dataSourceRepository,
                        RecentNewsWireframe wireframe) {
        this.dataSourceRepository = dataSourceRepository;
        this.wireframe = wireframe;
    }

    void retrieveNews(final RecentNewsState state) {
        if (!state.isRestarted() && state.isJustInitialized()) {
            getView().displayProgress(true);
            newsDisposable = dataSourceRepository.observeNews()
                    .subscribe(newsResponse -> {
                                getView().displayProgress(false);

                                getView().displayNews(new RecentNewsState.Builder(state)
                                        .isJustInitialized(false)
                                        .withResponse(newsResponse)
                                        .build());
                            },
                            throwable -> {
                                getView().displayProgress(false);
                                getView().displayError(throwable.getMessage());
                            });
        } else {
            getView().displayNews(state);
        }
    }

    void updateToolbarTile() {
        wireframe.updateToolbarTitle(R.string.crypto_news, false);
    }

    void openDetailsScreen(Article article) {
        wireframe.startDetailsScreen(article);
    }

    @Override
    public void dropView(RecentNewsStateView view) {
        super.dropView(view);
        if (newsDisposable != null) {
            newsDisposable.dispose();
            newsDisposable = null;
        }
    }
}
