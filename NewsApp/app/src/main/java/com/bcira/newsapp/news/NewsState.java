package com.bcira.newsapp.news;

import android.os.Parcel;
import android.os.Parcelable;

public class NewsState implements Parcelable {

    public static final Creator<NewsState> CREATOR = new Creator<NewsState>() {
        @Override
        public NewsState createFromParcel(Parcel in) {
            return new NewsState(in);
        }

        @Override
        public NewsState[] newArray(int size) {
            return new NewsState[size];
        }
    };
    private boolean isRestarted;

    protected NewsState(Builder builder) {
        isRestarted = builder.isRestarted;
    }

    protected NewsState(Parcel in) {
        this.isRestarted = in.readByte() != 0;
    }

    public boolean isRestarted() {
        return isRestarted;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isRestarted ? (byte) 1 : (byte) 0);
    }

    public static class Builder {
        private boolean isRestarted;

        public Builder() {
        }

        public Builder isRestarted(boolean val) {
            isRestarted = val;
            return this;
        }

        public NewsState build() {
            return new NewsState(this);
        }
    }
}
