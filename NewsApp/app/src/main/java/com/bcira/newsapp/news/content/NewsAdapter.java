package com.bcira.newsapp.news.content;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bcira.newsapp.R;
import com.bcira.newsapp.data.response.Article;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsItemViewHolder> {

    private LayoutInflater layoutInflater;
    private List<Article> articles;
    private ItemClickListener listener;

    NewsAdapter(Context context, List<Article> articles, ItemClickListener listener) {
        this.articles = articles;
        this.listener = listener;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public NewsItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View view = layoutInflater.inflate(R.layout.item_news, parent, false);
        return new NewsItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsItemViewHolder holder, int position) {
        holder.bind(articles.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }
}
