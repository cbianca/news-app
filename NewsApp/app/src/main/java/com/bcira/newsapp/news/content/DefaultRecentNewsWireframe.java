package com.bcira.newsapp.news.content;

import android.support.v4.app.FragmentActivity;

import com.bcira.newsapp.data.response.Article;
import com.bcira.newsapp.news.ScreenOpenerListener;
import com.bcira.newsapp.news.ToolbarTitleListener;

import java.lang.ref.WeakReference;

public class DefaultRecentNewsWireframe implements RecentNewsWireframe {

    private WeakReference<RecentNewsFragment> fragmentWeakReference;

    public DefaultRecentNewsWireframe(RecentNewsFragment fragment) {
        fragmentWeakReference = new WeakReference<>(fragment);
    }

    @Override
    public void updateToolbarTitle(int titleRes, boolean isHomeButtonEnabled) {
        FragmentActivity activity = fragmentWeakReference.get().getActivity();
        if (activity instanceof ToolbarTitleListener) {
            ((ToolbarTitleListener) activity).onTitleChanged(titleRes, isHomeButtonEnabled);
        }
    }

    @Override
    public void startDetailsScreen(Article article) {
        FragmentActivity activity = fragmentWeakReference.get().getActivity();
        if (activity instanceof ScreenOpenerListener) {
            ((ScreenOpenerListener) activity).openDetailsScreen(article);
        }
    }
}
