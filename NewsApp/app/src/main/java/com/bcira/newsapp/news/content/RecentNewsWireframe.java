package com.bcira.newsapp.news.content;

import com.bcira.newsapp.data.response.Article;

public interface RecentNewsWireframe {

    void updateToolbarTitle(int titleRes, boolean isHomeButtonEnabled);

    void startDetailsScreen(Article article);
}
