package com.bcira.newsapp.data.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Article implements Parcelable {

    public static final Parcelable.Creator<Article> CREATOR = new Parcelable.Creator<Article>() {
        @Override
        public Article createFromParcel(Parcel source) {
            return new Article(source);
        }

        @Override
        public Article[] newArray(int size) {
            return new Article[size];
        }
    };
    @SerializedName("author")
    private String author;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("content")
    private String content;
    @SerializedName("publishedAt")
    private Date publishedDate;
    @SerializedName("urlToImage")
    private String urlToImage;

    public Article() {
    }

    protected Article(Parcel in) {
        this.author = in.readString();
        this.title = in.readString();
        this.description = in.readString();
        this.content = in.readString();
        long tmpPublishedDate = in.readLong();
        this.publishedDate = tmpPublishedDate == -1 ? null : new Date(tmpPublishedDate);
        this.urlToImage = in.readString();
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getContent() {
        return content;
    }

    public Date getPublishedDate() {
        return publishedDate;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.author);
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.content);
        dest.writeLong(this.publishedDate != null ? this.publishedDate.getTime() : -1);
        dest.writeString(this.urlToImage);
    }
}
