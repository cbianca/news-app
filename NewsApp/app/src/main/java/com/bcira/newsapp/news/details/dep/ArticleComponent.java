package com.bcira.newsapp.news.details.dep;

import com.bcira.newsapp.news.dep.NewsComponent;
import com.bcira.newsapp.news.details.ArticleDetailsFragment;

import dagger.Component;

@ArticleScope
@Component(dependencies = NewsComponent.class, modules = ArticleModule.class)
public interface ArticleComponent {

    void inject(ArticleDetailsFragment fragment);
}
