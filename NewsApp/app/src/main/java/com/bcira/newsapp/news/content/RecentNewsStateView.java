package com.bcira.newsapp.news.content;

public interface RecentNewsStateView {

    void displayNews(RecentNewsState state);

    void displayError(String message);

    void displayProgress(boolean shouldDisplayProgress);
}
