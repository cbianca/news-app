package com.bcira.newsapp.news.content;

import com.bcira.newsapp.data.response.Article;

public interface ItemClickListener {

    void onItemClick(Article article);
}
