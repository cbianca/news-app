package com.bcira.newsapp.news.dep;

import com.bcira.newsapp.data.CryptoCoinsNewsApi;
import com.bcira.newsapp.dep.AppComponent;
import com.bcira.newsapp.news.NewsActivity;
import com.bcira.newsapp.util.RxSchedulers;

import dagger.Component;

@NewsScope
@Component(dependencies = AppComponent.class, modules = NewsModule.class)
public interface NewsComponent {

    void inject(NewsActivity activity);

    RxSchedulers rxSchedulers();

    CryptoCoinsNewsApi cryptoCoinsNewsApi();
}
