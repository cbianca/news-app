package com.bcira.newsapp.dep;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import com.bcira.newsapp.NewsApplication;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private NewsApplication application;

    public AppModule(NewsApplication application) {
        this.application = application;
    }

    @AppScope
    @Provides
    Application provideApplication() {
        return application;
    }

    @AppScope
    @Provides
    Context provideApplicationContext() {
        return application.getApplicationContext();
    }

    @AppScope
    @Provides
    Resources provideResources(Context context) {
        return context.getResources();
    }
}
