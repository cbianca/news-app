package com.bcira.newsapp.news.content.dep;

import com.bcira.newsapp.news.content.RecentNewsFragment;
import com.bcira.newsapp.news.dep.NewsComponent;

import dagger.Component;

@RecentNewsScope
@Component(dependencies = NewsComponent.class, modules = RecentNewsModule.class)
public interface RecentNewsComponent {

    void inject(RecentNewsFragment fragment);
}
