package com.bcira.newsapp.news.content.dep;

import com.bcira.newsapp.news.content.DefaultRecentNewsWireframe;
import com.bcira.newsapp.news.content.RecentNewsFragment;
import com.bcira.newsapp.news.content.RecentNewsWireframe;

import dagger.Module;
import dagger.Provides;

@Module
public class RecentNewsModule {

    private RecentNewsFragment fragment;

    public RecentNewsModule(RecentNewsFragment fragment) {
        this.fragment = fragment;
    }

    @RecentNewsScope
    @Provides
    RecentNewsWireframe provideRecentNewsWireframe() {
        return new DefaultRecentNewsWireframe(fragment);
    }
}
