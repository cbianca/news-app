package com.bcira.newsapp.common;

import java.lang.ref.WeakReference;

public abstract class BasePresenter<V> {

    private WeakReference<V> view = null;

    public void takeView(V view) {
        if (view == null) {
            throw new IllegalArgumentException("view shouldn't be null");
        }

        if (this.view != null) {
            dropView(this.view.get());
        }

        this.view = new WeakReference<>(view);
    }

    public V getView() {
        if (view == null) {
            throw new IllegalArgumentException("getView() called when view is null."
                    + "Ensure takeView(View view) is called first.");
        }
        return view.get();
    }

    public void dropView(V view) {
        if (view == null) {
            throw new IllegalArgumentException("dropped view shouldn't be null");
        }
        this.view.clear();
        this.view = null;
    }
}
