package com.bcira.newsapp;

import android.app.Application;

import com.bcira.newsapp.util.Injector;

public class NewsApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Injector.createAppComponent(this).inject(this);
    }
}
