package com.bcira.newsapp.dep;

import com.bcira.newsapp.NewsApplication;
import com.bcira.newsapp.data.CryptoCoinsNewsApi;
import com.bcira.newsapp.data.dep.NetworkModule;
import com.bcira.newsapp.util.RxSchedulers;

import dagger.Component;

@AppScope
@Component(modules = {AppModule.class, NetworkModule.class, RxModule.class})
public interface AppComponent {

    void inject(NewsApplication application);

    RxSchedulers rxSchedulers();

    CryptoCoinsNewsApi cryptoCoinsNewsApi();
}
