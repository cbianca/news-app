package com.bcira.newsapp.news;

import com.bcira.newsapp.data.response.Article;

public interface ScreenOpenerListener {

    void openDetailsScreen(Article article);
}
