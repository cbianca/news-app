package com.bcira.newsapp.dep;

import com.bcira.newsapp.util.AppRxSchedulers;
import com.bcira.newsapp.util.RxSchedulers;

import dagger.Module;
import dagger.Provides;

@Module
public class RxModule {

    @AppScope
    @Provides
    public RxSchedulers provideAppRxSchedulers() {
        return new AppRxSchedulers();
    }
}
