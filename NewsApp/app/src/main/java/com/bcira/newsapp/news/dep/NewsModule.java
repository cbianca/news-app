package com.bcira.newsapp.news.dep;

import com.bcira.newsapp.news.DefaultNewsWireframe;
import com.bcira.newsapp.news.NewsActivity;
import com.bcira.newsapp.news.NewsWireframe;

import dagger.Module;
import dagger.Provides;

@Module
public class NewsModule {

    private NewsActivity activity;

    public NewsModule(NewsActivity activity) {
        this.activity = activity;
    }

    @NewsScope
    @Provides
    NewsWireframe provideNewsWireframe() {
        return new DefaultNewsWireframe(activity);
    }
}
