package com.bcira.newsapp.news.details;

import android.support.v4.app.FragmentActivity;

import com.bcira.newsapp.news.ToolbarTitleListener;

import java.lang.ref.WeakReference;

public class DefaultArticleDetailsWireframe implements ArticleDetailsWireframe {

    private WeakReference<ArticleDetailsFragment> fragmentWeakReference;

    public DefaultArticleDetailsWireframe(ArticleDetailsFragment fragment) {
        fragmentWeakReference = new WeakReference<>(fragment);
    }

    @Override
    public void updateToolbarTitle(int titleRes, boolean isHomeButtonEnabled) {
        FragmentActivity activity = fragmentWeakReference.get().getActivity();
        if (activity instanceof ToolbarTitleListener) {
            ((ToolbarTitleListener) activity).onTitleChanged(titleRes, isHomeButtonEnabled);
        }
    }
}
