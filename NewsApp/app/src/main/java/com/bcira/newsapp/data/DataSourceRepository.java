package com.bcira.newsapp.data;

import com.bcira.newsapp.data.response.NewsResponse;

import javax.inject.Inject;

import io.reactivex.Single;

public class DataSourceRepository {

    private CryptoCoinsNewsRepository cryptoCoinsNewsRepository;

    @Inject
    DataSourceRepository(CryptoCoinsNewsRepository cryptoCoinsNewsRepository) {
        this.cryptoCoinsNewsRepository = cryptoCoinsNewsRepository;
    }

    public Single<NewsResponse> observeNews() {
        return cryptoCoinsNewsRepository.observeNews();
    }
}
