package com.bcira.newsapp.data;

import java.lang.reflect.Type;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.CallAdapter;

public class RxCallAdapterWrapper<R> implements CallAdapter<R, Object> {

    private final CallAdapter<R, Object> wrapped;

    RxCallAdapterWrapper(CallAdapter<R, Object> wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public Type responseType() {
        return wrapped.responseType();
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object adapt(Call<R> call) {
        Object result = wrapped.adapt(call);

        if (result instanceof Single) {
            result = ((Single) result).onErrorResumeNext(throwable ->
                    Single.error((Throwable) throwable));
        }

        return result;
    }
}
