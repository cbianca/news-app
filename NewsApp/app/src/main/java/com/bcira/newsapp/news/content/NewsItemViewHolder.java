package com.bcira.newsapp.news.content;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bcira.newsapp.R;
import com.bcira.newsapp.data.response.Article;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

class NewsItemViewHolder extends RecyclerView.ViewHolder {

    private static final String PATTERN = "dd MMM yyyy";

    @BindView(R.id.textview_title)
    TextView titleTextView;
    @BindView(R.id.textview_author)
    TextView authorTextView;
    @BindView(R.id.textview_date)
    TextView dateTextView;

    NewsItemViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @SuppressLint("SimpleDateFormat")
    void bind(Article article, ItemClickListener listener) {
        titleTextView.setText(article.getTitle());
        authorTextView.setText(article.getAuthor());
        SimpleDateFormat dateFormat = new SimpleDateFormat(PATTERN);
        dateTextView.setText(dateFormat.format(article.getPublishedDate()));
        itemView.setOnClickListener(v -> listener.onItemClick(article));
    }
}
