package com.bcira.newsapp.data.dep;

import android.util.Log;

import com.bcira.newsapp.data.CryptoCoinsNewsApi;
import com.bcira.newsapp.data.RxErrorHandlingCallAdapterFactory;
import com.bcira.newsapp.dep.AppScope;
import com.bcira.newsapp.util.Settings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bcira.newsapp.util.Settings.TIMEOUT;

@Module
public class NetworkModule {

    private static final String TAG = NetworkModule.class.getSimpleName();

    @AppScope
    @Provides
    Gson provideGson() {
        return new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    @AppScope
    @Provides
    OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS);

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(message ->
                Log.d(TAG, message));
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return builder.addInterceptor(httpLoggingInterceptor).build();
    }

    @AppScope
    @Provides
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(Settings.BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @AppScope
    @Provides
    CryptoCoinsNewsApi provideCryptoCoinsNewsApi(Retrofit retrofit) {
        return retrofit.create(CryptoCoinsNewsApi.class);
    }

}
