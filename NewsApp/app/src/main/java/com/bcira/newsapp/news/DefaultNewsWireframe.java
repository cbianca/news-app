package com.bcira.newsapp.news;

import android.support.v4.app.FragmentActivity;

import com.bcira.newsapp.R;
import com.bcira.newsapp.data.response.Article;
import com.bcira.newsapp.news.content.RecentNewsFragment;
import com.bcira.newsapp.news.details.ArticleDetailsFragment;

import java.lang.ref.WeakReference;

public class DefaultNewsWireframe implements NewsWireframe {

    private WeakReference<NewsActivity> activityWeakReference;

    public DefaultNewsWireframe(NewsActivity activity) {
        this.activityWeakReference = new WeakReference<>(activity);
    }

    @Override
    public void startNewsScreen() {
        FragmentActivity activity = activityWeakReference.get();
        if (activity != null) {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, RecentNewsFragment.newInstance())
                    .commit();
        }
    }

    @Override
    public void startDetailsScreen(Article article) {
        FragmentActivity activity = activityWeakReference.get();
        if (activity != null) {
            activity.getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.frame_container, ArticleDetailsFragment.newInstance(article))
                    .addToBackStack(ArticleDetailsFragment.TAG)
                    .commit();
        }
    }
}
