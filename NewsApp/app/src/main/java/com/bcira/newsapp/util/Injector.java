package com.bcira.newsapp.util;

import android.util.Log;

import com.bcira.newsapp.NewsApplication;
import com.bcira.newsapp.dep.AppComponent;
import com.bcira.newsapp.dep.AppModule;
import com.bcira.newsapp.dep.DaggerAppComponent;
import com.bcira.newsapp.news.NewsActivity;
import com.bcira.newsapp.news.content.RecentNewsFragment;
import com.bcira.newsapp.news.content.dep.DaggerRecentNewsComponent;
import com.bcira.newsapp.news.content.dep.RecentNewsComponent;
import com.bcira.newsapp.news.content.dep.RecentNewsModule;
import com.bcira.newsapp.news.dep.DaggerNewsComponent;
import com.bcira.newsapp.news.dep.NewsComponent;
import com.bcira.newsapp.news.dep.NewsModule;
import com.bcira.newsapp.news.details.ArticleDetailsFragment;
import com.bcira.newsapp.news.details.dep.ArticleComponent;
import com.bcira.newsapp.news.details.dep.ArticleModule;
import com.bcira.newsapp.news.details.dep.DaggerArticleComponent;

public enum Injector {

    INSTANCE;

    private static final String TAG = Injector.class.getSimpleName();
    private static final Object LOCK = new Object();

    private AppComponent appComponent;
    private NewsComponent newsComponent;
    private RecentNewsComponent recentNewsComponent;
    private ArticleComponent articleComponent;

    public static synchronized AppComponent createAppComponent(NewsApplication application) {
        if (INSTANCE.appComponent == null) {
            synchronized (LOCK) {
                if (INSTANCE.appComponent == null) {
                    Log.d(TAG, "createAppComponent");
                    INSTANCE.appComponent = DaggerAppComponent.builder()
                            .appModule(new AppModule(application))
                            .build();
                }
            }
        }
        return INSTANCE.appComponent;
    }

    //region NEWS
    public static synchronized NewsComponent createNewsComponent(NewsActivity activity) {
        if (INSTANCE.newsComponent == null) {
            synchronized (LOCK) {
                if (INSTANCE.newsComponent == null) {
                    Log.d(TAG, "createNewsComponent");
                    INSTANCE.newsComponent = DaggerNewsComponent.builder()
                            .appComponent(INSTANCE.appComponent)
                            .newsModule(new NewsModule(activity))
                            .build();
                }
            }
        }
        return INSTANCE.newsComponent;
    }

    public static synchronized NewsComponent getNewsComponent() {
        synchronized (LOCK) {
            return INSTANCE.newsComponent;
        }
    }

    public static synchronized void releaseNewsComponent() {
        synchronized (LOCK) {
            Log.d(TAG, "releaseNewsComponent");
            INSTANCE.newsComponent = null;
        }
    }
    //endregion

    //region RECENT NEWS
    public static synchronized RecentNewsComponent createRecentNewsComponent(RecentNewsFragment fragment) {
        if (INSTANCE.recentNewsComponent == null) {
            synchronized (LOCK) {
                if (INSTANCE.recentNewsComponent == null) {
                    Log.d(TAG, "createRecentNewsComponent");
                    INSTANCE.recentNewsComponent = DaggerRecentNewsComponent.builder()
                            .newsComponent(getNewsComponent())
                            .recentNewsModule(new RecentNewsModule(fragment))
                            .build();
                }
            }
        }
        return INSTANCE.recentNewsComponent;
    }

    public static synchronized RecentNewsComponent getRecentNewsComponent() {
        synchronized (LOCK) {
            return INSTANCE.recentNewsComponent;
        }
    }

    public static synchronized void releaseRecentNewsComponent() {
        synchronized (LOCK) {
            Log.d(TAG, "releaseRecentNewsComponent");
            INSTANCE.recentNewsComponent = null;
        }
    }
    //endregion

    //region ARTICLE
    public static synchronized ArticleComponent createArticleComponent(ArticleDetailsFragment fragment) {
        if (INSTANCE.articleComponent == null) {
            synchronized (LOCK) {
                if (INSTANCE.articleComponent == null) {
                    Log.d(TAG, "createArticleComponent");
                    INSTANCE.articleComponent = DaggerArticleComponent.builder()
                            .newsComponent(getNewsComponent())
                            .articleModule(new ArticleModule(fragment))
                            .build();
                }
            }
        }
        return INSTANCE.articleComponent;
    }

    public static synchronized void releaseArticleComponent() {
        synchronized (LOCK) {
            Log.d(TAG, "releaseArticleComponent");
            INSTANCE.articleComponent = null;
        }
    }
    //endregion
}
